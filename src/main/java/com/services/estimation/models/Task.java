package com.services.estimation.models;

import javax.validation.constraints.NotNull;

public class Task {

    @NotNull
    private Long id;

    @NotNull
    private Float userEstimation;

    private Double aiEstimation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getUserEstimation() {
        return userEstimation;
    }

    public Double getAiEstimation() {
        return aiEstimation;
    }

    public void setAiEstimation(Double aiEstimation) {
        this.aiEstimation = aiEstimation;
    }

    public void setUserEstimation(Float userEstimation) {
        this.userEstimation = userEstimation;
    }

}
