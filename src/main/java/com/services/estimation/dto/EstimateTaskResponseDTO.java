package com.services.estimation.dto;

public class EstimateTaskResponseDTO {
    private Long id;
    private Float aiEstimation;

    public EstimateTaskResponseDTO() {
    }

    public EstimateTaskResponseDTO(Long id, Float aiEstimation) {
        this.id = id;
        this.aiEstimation = aiEstimation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAiEstimation() {
        return aiEstimation;
    }

    public void setAiEstimation(Float aiEstimation) {
        this.aiEstimation = aiEstimation;
    }
}
