package com.services.estimation.dto;

public class EstimateTaskRequestDTO {

    private Long id;
    private Float userEstimation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getUserEstimation() {
        return userEstimation;
    }

    public void setUserEstimation(Float userEstimation) {
        this.userEstimation = userEstimation;
    }
}
