package com.services.estimation.controllers;

import com.services.estimation.dto.EstimateTaskRequestDTO;
import com.services.estimation.services.EstimationService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("estimate")
public class EstimationController {

    private final EstimationService estimationService;

    @Autowired
    public EstimationController(EstimationService estimationService) {
        this.estimationService = estimationService;
    }

    @RabbitListener(queues = "${rabbitmq.tasks.queue}")
    public void listen(EstimateTaskRequestDTO in) {
        this.estimationService.estimate(in);
    }
}
