package com.services.estimation.services;

import com.services.estimation.dto.EstimateTaskRequestDTO;
import com.services.estimation.dto.EstimateTaskResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EstimationService {

    private TasksService tasksService;

    @Autowired
    public EstimationService(TasksService tasksService) {
        this.tasksService = tasksService;
    }

    public void estimate(EstimateTaskRequestDTO requestDTO) {
        double aiEstimation = requestDTO.getUserEstimation() * 1.3;

        EstimateTaskResponseDTO responseDTO = new EstimateTaskResponseDTO();
        responseDTO.setId(requestDTO.getId());
        responseDTO.setAiEstimation(Float.parseFloat(Double.toString(aiEstimation)));
        System.out.println("Estimating task id: " + requestDTO.getId());
        this.tasksService.updateEstimation(requestDTO.getId(), responseDTO);
    }
}
