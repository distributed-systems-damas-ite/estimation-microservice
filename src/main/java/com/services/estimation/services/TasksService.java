package com.services.estimation.services;

import com.services.estimation.dto.EstimateTaskResponseDTO;
import com.services.estimation.models.Task;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "taskservice", fallback = TaskServiceFallbackImpl.class)
public interface TasksService {
    @RequestMapping(value = "/tasks/estimate/{id}", method = RequestMethod.POST)
    Task updateEstimation(@PathVariable long id, @RequestBody EstimateTaskResponseDTO task);
}
