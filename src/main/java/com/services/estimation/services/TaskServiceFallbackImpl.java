package com.services.estimation.services;

import com.services.estimation.dto.EstimateTaskResponseDTO;
import com.services.estimation.models.Task;
import org.springframework.stereotype.Component;

@Component
class TaskServiceFallbackImpl implements TasksService {
    @Override
    public Task updateEstimation(long id, EstimateTaskResponseDTO task) {
        return new Task();
    }
}